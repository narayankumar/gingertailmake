; Drupal BOA core
core = "7.x"
api = 2
projects[drupal][type] = "core"
projects[drupal][download][type] = "get"
projects[drupal][download][url] = "http://files.aegir.cc/core/drupal-7.41.1.tar.gz"

; Modules
projects[addressfield][version] = 1.2
projects[addressfield][subdir] = "contrib"

projects[addthis][version] = 4.0-alpha6
projects[addthis][subdir] = "contrib"

projects[admin_menu][version] = 3.0-rc5
projects[admin_menu][subdir] = "contrib"

projects[advanced_forum][version] = 2.6
projects[advanced_forum][subdir] = "contrib"

projects[auto_nodetitle][version] = 1.0
projects[auto_nodetitle][subdir] = "contrib"

projects[backup_migrate][version] = 2.8
projects[backup_migrate][subdir] = "contrib"

projects[colorbox][version] = 2.9
projects[colorbox][subdir] = "contrib"

; projects[colorbox][version] = "2.10"
; projects[colorbox][patch][] = "http://www.drupal.org/files/issues/Colorbox_plugin_not_detected-2336341-5.patch"

projects[conditional_fields][version] = 3.0-alpha2
projects[conditional_fields][subdir] = "contrib"

projects[context][version] = 3.6
projects[context][subdir] = "contrib"

projects[context_admin][version] = 1.2
projects[context_admin][subdir] = "contrib"

projects[ctools][version] = 1.9
projects[ctools][subdir] = "contrib"

projects[date][version] = 2.9
projects[date][subdir] = "contrib"

projects[devel][version] = 1.5
projects[devel][subdir] = "contrib"

projects[devel_themer][version] = 1.x-dev
projects[devel_themer][subdir] = "contrib"

projects[diff][version] = 3.2
projects[diff][subdir] = "contrib"

projects[elements][version] = 1.4
projects[elements][subdir] = "contrib"

projects[email][version] = 1.3
projects[email][subdir] = "contrib"

projects[entityreference][version] = 1.1
projects[entityreference][subdir] = "contrib"

projects[entityreference_prepopulate][version] = 1.5
projects[entityreference_prepopulate][subdir] = "contrib"

projects[eva][version] = 1.2
projects[eva][subdir] = "contrib"

projects[entity][version] = 1.6
projects[entity][subdir] = "contrib"

; download this separately after build is made
; projects[fbconnect][version] = 2.0-beta4
; projects[fbconnect][subdir] = "contrib"

projects[fboauth][version] = 2.0-rc1
projects[fboauth][subdir] = "contrib"

projects[facetapi][version] = 1.5
projects[facetapi][subdir] = "contrib"

projects[features][version] = 2.7
projects[features][subdir] = "contrib"

projects[features_extra][version] = 1.0
projects[features_extra][subdir] = "contrib"

projects[feeds][version] = 2.0-beta1
projects[feeds][subdir] = "contrib"

projects[feeds_tamper][version] = 1.1
projects[feeds_tamper][subdir] = "contrib"

projects[field_collection][version] = 1.0-beta10
projects[field_collection][subdir] = "contrib"

projects[field_hidden][version] = 1.7
projects[field_hidden][subdir] = "contrib"

projects[file_entity][version] = 2.0-beta2
projects[file_entity][subdir] = "contrib"

projects[fivestar][version] = 2.1
projects[fivestar][subdir] = "contrib"

projects[flag][version] = 3.7
projects[flag][subdir] = "contrib"

projects[geocoder][version] = 1.2
projects[geocoder][subdir] = "contrib"

projects[geofield][version] = 2.3
projects[geofield][subdir] = "contrib"

projects[geophp][version] = 1.7
projects[geophp][subdir] = "contrib"

projects[google_analytics][version] = 2.1
projects[google_analytics][subdir] = "contrib"

projects[html5_tools][version] = 1.3
projects[html5_tools][subdir] = "contrib"

projects[imagecache_actions][version] = 1.5
projects[imagecache_actions][subdir] = "contrib"

projects[imagecache_token][version] = 1.0-rc1
projects[imagecache_token][subdir] = "contrib"

projects[imce][version] = 1.9
projects[imce][subdir] = "contrib"

projects[imce_wysiwyg][version] = 1.0
projects[imce_wysiwyg][subdir] = "contrib"

projects[jquery_colorpicker][version] = 1.2
projects[jquery_colorpicker][subdir] = "contrib"

projects[jquery_update][version] = 2.6
projects[jquery_update][subdir] = "contrib"

projects[job_scheduler][version] = 2.0-alpha3
projects[job_scheduler][subdir] = "contrib"

projects[leaflet][version] = 1.3
projects[leaflet][subdir] = "contrib"

projects[leaflet_more_maps][version] = 1.14
projects[leaflet_more_maps][subdir] = "contrib"

projects[legal][version] = 1.5
projects[legal][subdir] = "contrib"

projects[less][version] = 3.0
projects[less][subdir] = "contrib"

projects[libraries][version] = 2.2
projects[libraries][subdir] = "contrib"

projects[link][version] = 1.3
projects[link][subdir] = "contrib"

projects[media_colorbox][version] = 1.0-rc4
projects[media_colorbox][subdir] = "contrib"

projects[metatag][version] = 1.7
projects[metatag][subdir] = "contrib"

projects[modal_forms][version] = 1.2
projects[modal_forms][subdir] = "contrib"

projects[module_filter][version] = 2.0
projects[module_filter][subdir] = "contrib"

projects[mollom][version] = 2.15
projects[mollom][subdir] = "contrib"

projects[nivo_slider][version] = 1.11
projects[nivo_slider][subdir] = "contrib"

projects[node_gallery][version] = 1.1
projects[node_gallery][subdir] = "contrib"

projects[nodequeue][version] = 2.0
projects[nodequeue][subdir] = "contrib"

projects[pathauto][version] = 1.3
projects[pathauto][subdir] = "contrib"

projects[panels][version] = 3.5
projects[panels][subdir] = "contrib"

projects[plupload][version] = 1.7
projects[plupload][subdir] = "contrib"

projects[publish_button][version] = 1.1
projects[publish_button][subdir] = "contrib"

projects[rules][version] = 2.9
projects[rules][subdir] = "contrib"

projects[search_api][version] = 1.16
projects[search_api][subdir] = "contrib"

projects[search_api_fivestar][version] = 1.x-dev
projects[search_api_fivestar][subdir] = "contrib"

projects[search_api_solr][version] = 1.9
projects[search_api_solr][subdir] = "contrib"

projects[session_api][version] = 1.0-rc1
projects[session_api][subdir] = "contrib"

projects[simplehtmldom][version] = 1.12
projects[simplehtmldom][subdir] = "contrib"

projects[site_map][version] = 1.2
projects[site_map][subdir] = "contrib"

projects[statuses][version] = 1.0-beta2
projects[statuses][subdir] = "contrib"

projects[strongarm][version] = 2.0
projects[strongarm][subdir] = "contrib"

projects[token][version] = 1.6
projects[token][subdir] = "contrib"

projects[uuid][version] = 1.0-beta1
projects[uuid][subdir] = "contrib"

projects[uuid_features][version] = 1.0-alpha4
projects[uuid_features][subdir] = "contrib"

projects[video_embed_field][version] = 2.0-beta11
projects[video_embed_field][subdir] = "contrib"

projects[views][version] = 3.13
projects[views][subdir] = "contrib"

projects[views_bulk_operations][version] = 3.3
projects[views_bulk_operations][subdir] = "contrib"

projects[views_fb_like][version] = 1.2-beta1
projects[views_fb_like][subdir] = "contrib"

projects[views_periodic_execution][version] = 1.3-beta1
projects[views_periodic_execution][subdir] = "contrib"

projects[voting_rules][version] = 1.0-alpha1
projects[voting_rules][subdir] = "contrib"

projects[vote_up_down][version] = 1.0-alpha1
projects[vote_up_down][subdir] = "contrib"

projects[votingapi][version] = 2.12
projects[votingapi][subdir] = "contrib"

projects[wysiwyg][version] = 2.2
projects[wysiwyg][subdir] = "contrib"

projects[xmlsitemap][version] = 2.2
projects[xmlsitemap][subdir] = "contrib"

; Features
; Custom gingertail modules

projects[ad][type] = "module"
projects[ad][version] = 1.0-dev1
projects[ad][subdir] = "custom"
projects[ad][download][type] = "git"
projects[ad][download][url] = "https://narayankumar@bitbucket.org/narayankumar/ad.git"

projects[adoption][type] = "module"
projects[adoption][version] = 1.0-dev1
projects[adoption][tag] = "build2014092102a"
projects[adoption][subdir] = "custom"
projects[adoption][download][type] = "git"
projects[adoption][download][url] = "https://narayankumar@bitbucket.org/narayankumar/adoption.git"

projects[announcement][type] = "module"
projects[announcement][version] = 1.0-dev1
projects[announcement][subdir] = "custom"
projects[announcement][download][type] = "git"
projects[announcement][download][url] = "https://narayankumar@bitbucket.org/narayankumar/announcement.git"

projects[article_plus][type] = "module"
projects[article_plus][branch] = "7.x-1.0-dev2"
projects[article_plus][tag] = "build2014092701"
projects[article_plus][subdir] = "custom"
projects[article_plus][download][type] = "git"
projects[article_plus][download][url] = "https://narayankumar@bitbucket.org/narayankumar/article-plus.git"

projects[directory][type] = "module"
projects[directory][version] = 1.0-dev1
projects[directory][tag] = "2014092101"
projects[directory][subdir] = "custom"
projects[directory][download][type] = "git"
projects[directory][download][url] = "https://narayankumar@bitbucket.org/narayankumar/directory.git"

projects[found_pet][type] = "module"
projects[found_pet][version] = 1.0-dev1
projects[found_pet][tag] = "build2014092101"
projects[found_pet][subdir] = "custom"
projects[found_pet][download][type] = "git"
projects[found_pet][download][url] = "https://narayankumar@bitbucket.org/narayankumar/found-pet.git"

projects[front_page][type] = "module"
projects[front_page][version] = 1.0-dev1
projects[front_page][subdir] = "custom"
projects[front_page][download][type] = "git"
projects[front_page][download][url] = "https://narayankumar@bitbucket.org/narayankumar/front-page.git"

projects[ginger_forums][type] = "module"
projects[ginger_forums][version] = 1.0-dev1
projects[ginger_forums][tag] = "build2014091901"
projects[ginger_forums][subdir] = "custom"
projects[ginger_forums][download][type] = "git"
projects[ginger_forums][download][url] = "https://narayankumar@bitbucket.org/narayankumar/ginger-forums.git"

projects[guest_column][type] = "module"
projects[guest_column][version] = 1.0-dev1
projects[guest_column][tag] = "build2014092101"
projects[guest_column][subdir] = "custom"
projects[guest_column][download][type] = "git"
projects[guest_column][download][url] = "https://narayankumar@bitbucket.org/narayankumar/expert-talk.git"

projects[lost_pet][type] = "module"
projects[lost_pet][version] = 1.0-dev1
projects[lost_pet][tag] = "build2014092102"
projects[lost_pet][subdir] = "custom"
projects[lost_pet][download][type] = "git"
projects[lost_pet][download][url] = "https://narayankumar@bitbucket.org/narayankumar/lost-pet.git"

projects[news_plus][type] = "module"
projects[news_plus][branch] = 7.x-1.0-dev2
projects[news_plus][tag] = "build2014092701"
projects[news_plus][subdir] = "custom"
projects[news_plus][download][type] = "git"
projects[news_plus][download][url] = "https://narayankumar@bitbucket.org/narayankumar/news-plus.git"

projects[opinion][type] = "module"
projects[opinion][branch] = 7.x-1.0-dev2
projects[opinion][tag] = "build2014092701"
projects[opinion][subdir] = "custom"
projects[opinion][download][type] = "git"
projects[opinion][download][url] = "https://narayankumar@bitbucket.org/narayankumar/opinion.git"

projects[pet_videos][type] = "module"
projects[pet_videos][version] = 1.0-dev1
projects[pet_videos][tag] = "build2014091802"
projects[pet_videos][subdir] = "custom"
projects[pet_videos][download][type] = "git"
projects[pet_videos][download][url] = "https://narayankumar@bitbucket.org/narayankumar/pet-videos.git"

projects[photo_albums][type] = "module"
projects[photo_albums][version] = 1.0-dev1
projects[photo_albums][tag] = "build2014091801"
projects[photo_albums][subdir] = "custom"
projects[photo_albums][download][type] = "git"
projects[photo_albums][download][url] = "https://narayankumar@bitbucket.org/narayankumar/photo-albums.git"

projects[photo_contest][type] = "module"
projects[photo_contest][version] = 1.0-dev1
projects[photo_contest][tag] = "build2014091901"
projects[photo_contest][subdir] = "custom"
projects[photo_contest][download][type] = "git"
projects[photo_contest][download][url] = "https://narayankumar@bitbucket.org/narayankumar/photo-contest.git"

projects[user_tabs][type] = "module"
projects[user_tabs][version] = 1.0-dev1
projects[user_tabs][tag] = "build2014092701"
projects[user_tabs][subdir] = "custom"
projects[user_tabs][download][type] = "git"
projects[user_tabs][download][url] = "https://narayankumar@bitbucket.org/narayankumar/user-tabs.git"

projects[video_contest][type] = "module"
projects[video_contest][version] = 1.0-dev1
projects[video_contest][tag] = "build2014091801"
projects[video_contest][subdir] = "custom"
projects[video_contest][download][type] = "git"
projects[video_contest][download][url] = "https://narayankumar@bitbucket.org/narayankumar/video-contest.git"

; Themes
; bootstrap
projects[bootstrap][type] = "theme"
projects[bootstrap][version] = "2.0-beta3"
projects[bootstrap][subdir] = "contrib"

; Custom themes
projects[porto][type] = "theme"
projects[porto][version] = 2.0.3
projects[porto][subdir] = "custom"
projects[porto][download][type] = "git"
projects[porto][download][url] = "https://narayankumar@bitbucket.org/narayankumar/porto.git"

projects[porto_sub][type] = "theme"
projects[porto_sub][version] = 1.0
projects[porto_sub][tag] = "build2014092701"
projects[porto_sub][subdir] = "custom"
projects[porto_sub][download][type] = "git"
projects[porto_sub][download][url] = "https://narayankumar@bitbucket.org/narayankumar/porto-sub.git"

; Libraries
libraries[colorbox][directory_name] = "colorbox"
libraries[colorbox][type] = "library"
libraries[colorbox][destination] = "libraries"
libraries[colorbox][download][type] = "get"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox/archive/1.x.zip"

libraries[libphonenumber-for-php][directory_name] = "libphonenumber-for-php"
libraries[libphonenumber-for-php][type] = "library"
libraries[libphonenumber-for-php][destination] = "libraries"
libraries[libphonenumber-for-php][download][type] = "git"
libraries[libphonenumber-for-php][download][url] = "https://github.com/chipperstudios/libphonenumber-for-php.git"

libraries[tinymce][directory_name] = "tinymce"
libraries[tinymce][type] = "library"
libraries[tinymce][destination] = "libraries"
libraries[tinymce][download][type] = "get"
libraries[tinymce][download][url] = "http://github.com/downloads/tinymce/tinymce/tinymce_3.5.8.zip"

libraries[bootstrap][directory_name] = "bootstrap"
libraries[bootstrap][type] = "library"
libraries[bootstrap][destination] = "themes/contrib/bootstrap/"
libraries[bootstrap][download][type] = "get"
libraries[bootstrap][download][url] = "https://github.com/twbs/bootstrap/archive/v3.0.3.zip"

libraries[plupload][directory_name] = "plupload"
libraries[plupload][type] = "library"
libraries[plupload][destination] = "libraries"
libraries[plupload][download][type] = "get"
libraries[plupload][download][url] = "https://github.com/moxiecode/plupload/archive/v1.5.8.zip"

libraries[leaflet][directory_name] = "leaflet"
libraries[leaflet][type] = "library"
libraries[leaflet][destination] = "libraries"
libraries[leaflet][download][type] = "get"
libraries[leaflet][download][url] = "http://cdn.leafletjs.com/downloads/leaflet-0.7.3.zip"

libraries[colorpicker][directory_name] = "colorpicker"
libraries[colorpicker][type] = "library"
libraries[colorpicker][destination] = "libraries"
libraries[colorpicker][download][type] = "get"
libraries[colorpicker][download][url] = "http://www.eyecon.ro/colorpicker/colorpicker.zip"

libraries[lessphp][directory_name] = "lessphp"
libraries[lessphp][type] = "library"
libraries[lessphp][destination] = "libraries"
libraries[lessphp][download][type] = "get"
libraries[lessphp][download][url] = "https://codeload.github.com/oyejorge/less.php/zip/master"

libraries[nivo-slider][directory_name] = "nivo-slider"
libraries[nivo-slider][type] = "library"
libraries[nivo-slider][destination] = "libraries"
libraries[nivo-slider][download][type] = "get"
libraries[nivo-slider][download][url] = "https://codeload.github.com/gilbitron/Nivo-Slider/zip/master"

libraries[facebook-php-sdk][directory_name] = "facebook-php-sdk"
libraries[facebook-php-sdk][type] = "library"
libraries[facebook-php-sdk][download][type] = "get"
libraries[facebook-php-sdk][download][url] = "http://github.com/facebook/facebook-php-sdk/tarball/v3.1.1"
libraries[facebook-php-sdk][destination] = "libraries"

