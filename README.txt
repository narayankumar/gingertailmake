********************************************************************************

- Set group writable PERMISSIONS for platform’s root directory, plus its sites
and core modules subdirectories with commands: 'chmod 775 ~/static/foo-bar'
plus 'chmod -R 775 ~/static/foo-bar/sites' and 'chmod 775 ~/static/foo-bar/modules'.
You must set this chmod before adding the platform in the Aegir interface,
or it will yell with errors or cause warnings later.
- DELETE examples folder in libraries/plupload folder

********************************************************************************
BETA1-build20141117
- metatag, imagecache_actions, imagecache_token modules added
- fbconnect removed, fboauth included
- drupal 7.32 upgrade from BOA

BETA1-build2014092701
- beta build that will transfer to LIVE site for first time
- porto_sub
   .region-sidebar-right span.views-field-field-guest-name {padding:0 0}
   .region-sidebar-right img {margin:0 0}
   #footer,
   #footer .footer-copyright {margin-top:0px;padding:0 0;}
   node--news.tpl.php changed - pet news changed to ginger news
   ginger news changed in node label
   node--article.tpl.php changed - pet articles changed to ginger digest
   ginger digest changed in node label
   node--opinion.tpl.php changed - your opinion changed to parent talk
   parent talk changed in node label
- article_plus 
   label of ct changed to 'ginger digest'
   removed comments field from front page teasers view
   removed comments field from front page news view
   removed comments field from front page voices view
- news_plus
   label of ct changed to 'ginger news'
- opinion
   label of ct changed to 'parent talk'
- user_tabs
   added panel of one more statuses view


build20140921b
- porto_sub
   article-row-front margin-right reduced to 5px
- adoption
   deleted duplicate entries in pet types taxonomy
   added pet types exposed filter in adoption teasers view


build20140921a
- changed tags on directory and porto_sub
- added css lines for right borders on front page blocks
- extra image field on directory CT deleted (missed out in the previous build)


build2014092101
- adoption build2014092101
   image field made compulsory
   flag's unflagging permissions only for admin, site admin and editor
   unflag text is 'adopted'
   rules message changed to 'pet adopted'
   views changes
   - image size in teasers to 16x110
   - adopted-field hide when empty (for css not to go haywire)
   - search by city exposed filter added to teasers view
- article_plus build2014092101
   views changes
   - items per page increased to 4 pet photos front page view
   - image style changed to node_gallery_file_thumbnail for above
- directory build2014092101
   extra image field deleted
- found_pet build2014092101
   image field made compulsory
   views changes
   - empty global area text deleted
   - comment field in teasers view deleted
   - field pet_reunited hide when empty (for css to work fine)
   - deleted body field from teasers view
   - included 'click for details' in teasers view (link to content field)
   - search by city exposed filter added to teasers view
- lost_pet build2014092101
   image field made compulsory
   removed 'more' link in page manager from lost-found page
   rules message changed to 're-united with owner'
   removed wrong field from teasers view, replaced with pet-found field
   search by city exposed filter added to teasers view
- expert talk build2014092101
   views changes
   - comments field in teasers view made into a link
- news_plus build2014092101
   views changes
   - comments field in teasers view made into a link
- porto_sub build2014092101
   css changes recorded in module version build2014092101


build2014091901
- .make file
   added views_fb_like module 1.2-beta1
   added views_periodic_execution 1.3-beta1 (dependency)
- porto_sub
   login msg for anon users on contest pages
   template file edits with new code
   new css to go with above: .login-line, .contest-closed {margin-bottom:25px}
- article_plus
   sort by fb likes
   change titles of all sub-views to 'trending…' instead of 'latest...'
- ginger_forums
   permissions added
- guest_column
   added opinion block for front page
- photo_contest
   added new view - trending contest entry blocks for photo and video on front page


build2014091802
- news
   change exposed filter label to 'news categories'
   replaced type in teasers view with news categories taxonomy term
- porto
   template.php includes btn-xs within button function
- pet_videos
   h4 title words thin and bold
- porto_sub
   custom.css changed body to 13px from 12
   change 'search by rating' fields to small caps
   .field-name-field-directory-geofield - remove 180px margin-top
   directory-bottom - clear left to make social icons start on fresh line
   change to node--directory-entry.tpl.php to shift images under map
- directory
   teasers view - global text area - see all products changed to see all
- guest_column
   replaced type in teasers view with voice categories taxonomy term
- article_plus
   replaced type in teasers view with article categories taxonomy term


build2014091801
- teasers pages carry title of h4 and black
- pathauto changes to voices
- various css changes
- affected modules are the following:

  porto_sub build2014091801
    - body {font-size: 13px;line-height: 20px;}
    - .body .form-text {height:24px}
    - .views-exposed-form .views-exposed-widget .form-submit {margin-top:2.1em}
    - div.view-filters {margin-bottom: 25px;padding-bottom:10px;border-bottom: 1px solid #dedede}
    - .page-voices ul.action-links // replace the guestcol line with this
    - .page-pet-photos ul.action-links // replace gallery line with this
    - .page-pet-adoption ul.action-links // replace adoption lines with this (2 lines)
    - delete: .form-managed-file .form-file {margin:10px}
    - change the following lines from the 'galleries' line:
    - .page-pet-photos #album {width:80%; padding: 0px 0px;margin-top:33px}  
    - .page-pet-photos div.container {padding: 0px 0px}
    - div.user-album {display:none} //hide 'user album' title
    - change adoption, lost pet, found pet templates to show red instead of green markup
    - .views-exposed-widget label {
      font-size: 11px;font-style: normal;
      line-height: 20px;
      margin-right: 3px;
      text-transform: uppercase;
      font-weight: 700;
      }

  news ct build2014091801
    - made title 'news' in teasers view
    - gave it class=black, h4 and strong

  article ct build2014091801
    - made title 'articles' in teasers view
    - gave it class=black, h4 and strong 
    - new path for article nodes

  expert talk ct build2014091801
    - made title 'voices' in teasers view
    - gave it class=black, h4, second word strong
    - changed page manager setting to show voices/add voices/manage etc.
    - new path for expert talk nodes

  photo ct build2014091801
    - made title 'pet photos' in teasers view (manual add)
    - gave it class=black, h4,strong
    - changed page manager setting to show pet-photos/add
    - changed path
    - changed name of CT to album instead of photo album

  opinion ct build2014091801
    - new path for opinion nodes

  pet videos build2014091801
    - made title 'pet videos' in teasers view
    - gave it class=back, h4, second word strong

  directory build2014091801
    - made title 'directory' in teasers view
    - gave it class=back, h4, strong

  lost pet build2014091801
    - made title 'pets lost' in teasers view
    - gave it class=back, h4, second word strong
    - teasers view - found message as red & strong 
    - changed link wording to 'report lost or missing pet'

  found pet build2014091801
    - made title 'pets found' in teasers view
    - gave it class=back, h4, second word strong
    - teasers view - re-united message as red & strong
    - added name and posted date to match lost pet view
    - changed link wording to 'report found pet'

  pet adoption build2014091801
    - changed title to 'pet adoption' in teasers view
    - gave it class=back, h4, second word strong
    - teasers view - re-united message as red & strong
    - changed link to pet-adoption
    - path change to pet-adoption
    - changed text in side-menu to 'pet adoption' (manual) and link inside block

  photo contest build2014091801
    - made title 'photo contest' in teasers view
    - gave it class=back, h4, second word strong 

  video contest build2014091801
    - made title 'video contest' in teasers view
    - gave it class=back, h4, second word strong 


build2014091602b
- added css for margin below category fields in opinion and opinion-invite nodes


build2014091602a
- news template updated to display news category on node
- similar for opinion and opinion-invite templates


build2014091602
- adoption build2014091602
- article_plus build2014091602
- expert talk build2014091602
- lost_pet build2014091602
- found_pet build2014091602
- invite opinion build2014091602
- news_plus build2014091602
- opinion build2014091602
- submit photo contest build2014091602
- submit video contest build2014091602
- mostly for hiding sharethis label for CTs above + providing add comment box instead of ajax link
- porto_sub build2014091602 with css for the above changes


build2014091601
- adoption build2014091601
   teasers view puts city on top of image
   adds name and date below image
- article_plus build2014091601
   sub-views under 'recent articles block' changed
   for photos on front page - image re-sized to 'teaser'
   for lost pet, found pet and adoption images on front page - image re-sized 165x110


build2014091502
pulled in changes from:
 - pet videos build2014091501
    teasers view to have share | comment instead of add comment. 
    also made comment form visible, not ajax
- photo albums 2014091501
    view make share|comment instead of add comment (manual)
    make comment form visible, no ajax
- directory build2014091502
    make comment form visible no ajax
    change download pdf to upload brochure
- user tabs build2014091501
    removed modal form for comment
    changed modal form login box dimensions
- video contest build2014091501
    text format plain text
    rows in body 5 from 3


build2014091201
- initial build on 12 sep
- added porto and porto_sub themes

